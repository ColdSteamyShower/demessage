// components
import HeaderBar from "../../components/HeaderBar"
import Messaging from "./components/Messaging/Messaging"
import SideBar from "./components/SideBar"

// modules
import { useEffect, useState, useReducer } from 'react'
import Gun from 'gun'

import { useWeb3React } from "@web3-react/core"
import { injected } from '../../components/wallet/connectors.js'

//styles
import "./Application.css"

// Gun.js context
const gun = Gun({
  peers: [
    'https://demessage-gun-node.herokuapp.com/gun',
    'http://localhost:9000/gun'
  ]
})

const initialState = {
  messages: []
}

function reducer(state, message) {
  return {
    messages: [ ...state.messages, message]
  }
}

const Application = () => {

  // wallet auth
  const {active, account, library, connector, activate, deactivate } = useWeb3React()

    const connect = async () => {
      try {
        await activate(injected)
      } catch (err) {
        console.log(err)
      }
    }

    const disconnect = () => {
      try {
        deactivate(injected)
      } catch (err) {
        console.log(err)
      }
    }

    // state for current message box
    const [formState, setForm] = useState({
      message: ''
    })
  
    // reducer to store initial state data
    const [state, dispatch] = useReducer(reducer, initialState)
  
    // when the app loads, fetch the current messages and load them into the state
    // this also subscribes to new data as it changes and updates the local state
    useEffect(() => {
      const messages = gun.get('messages')
      messages.map().once(m => {
        dispatch({
          name: m.name,
          message: m.message,
          createdAt: m.createdAt
        })
      })
    }, [])
  
    // set a new message in gun, update the local state to reset the form field
    function saveMessage() {
      const messages = gun.get('messages')
      messages.set({
        name: active ? account : 'null user',
        message: formState.message,
        createdAt: Date.now()
      })
      setForm({
        message: ''
      })
    }

    return (
        <div className="d-flex flex-column">
            <HeaderBar/>
            <div className="row w-100 m-0 d-flex application-box">
                <div className="col-3 p-0">
                    <SideBar
                      connect={connect}
                      disconnect={disconnect}
                      account={account}
                      active={active}
                      />
                </div>

                <div className="col-9 p-0 messaging-component">
                    <Messaging
                      state={state}
                      account={account}
                      send={saveMessage}
                      formState={formState}
                      setForm={setForm}
                    />
                </div>
            </div>
        </div>
    )
}
export default Application