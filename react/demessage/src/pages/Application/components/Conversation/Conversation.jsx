import React from 'react'
import './Conversation.css'

const Conversation = (props) => {
    return <>
        <div className="h-25 d-flex flex-row conversation rounded user-select-none cursor-pointer">
            <div className=' p-2 w-35 h-100'>
                <img className="h-100 bg-lilac rounded-circle" src={props.image}/>
            </div>
            <div className='w-65 h-100 d-flex align-items-center'>
                <p className='text-white'>{props.name}</p>
            </div>
        </div>
        

    </>
}
export default Conversation