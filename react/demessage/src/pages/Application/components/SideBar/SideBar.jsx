import {useState} from 'react'

// components
import Conversation from '../Conversation'

// styles
import './SideBar.css'

// resources
import MetaMaskFox from './MetaMask_Fox.png'

// demo data
const dummyChannels = [
    {
        id: 1,
        name: 'Public',
        avatar: MetaMaskFox,
    }
]

const SideBar = (props) => {
    
    const [channels, setChannels] = useState(dummyChannels);
    


    return <>
        {(props.active) ? 
            <div className="w-100 h-100 bg-purple justify-content-start align-items-center d-flex flex-column sidebar">
                <img className="bg-lilac userProfilePicture border border-denim border-3 rounded-circle mt-3 mb-3" src={MetaMaskFox} alt="user img"/>
                <p className="w-85 text-lilac text-center text-truncate text-lg-h4 flex-shrink-0">DEMO ID</p>
                <p className="w-85 text-orchid text-center text-truncate text-lg-small flex-shrink-0">Wallet ID: {props.account}</p>
                <button 
                    className="bg-turquoise w-85 mt-3 p-2 rounded text-denim text-lg-h6" 
                    onClick={(props.disconnect)}
                >
                    Log out of MetaMask
                </button>
                <p className='mt-4 pb-1 border-bottom w-85 text-center text-lg-h4 text-turquoise user-select-none'>Text Channels</p>
                <div className='w-85 pe-1 flex flex-row flex-grow-1 flex-shrink-1 my-3 conversations'>
                    {channels.map((channel, index) => {
                        return <Conversation 
                            key={index}
                            id={channel.id}
                            name={channel.name}
                            image={channel.avatar}
                        />
                    })}
                </div>
            </div>
        :
            <div className="w-100 h-100 bg-purple justify-content-center align-items-start d-flex">
                <button 
                    className="bg-turquoise w-85 mt-5 p-1 rounded text-denim text-lg-h3" 
                    onClick={(props.connect)}
                >
                    <img src={MetaMaskFox} alt="metamask fox" className="metamask"/>
                    Login with MetaMask
                </button>
            </div>

        }

    </>
}
export default SideBar