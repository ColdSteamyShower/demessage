import "./Messaging.css"

import { useEffect, useRef } from "react";

const Messaging = (props) => {
    
    const sendPrompt = () => {
       props.send()
    }

    const messagesEndRef = useRef(null)

    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
      }
    
    useEffect(() => {
        scrollToBottom()
    }, [props.state]);

    return (
        <div className="bg-turquoise message-window">
            <div className="p-4 pb-0 m-0 chatbounds">
                <div className="chatbox bg-lilac border rounded p-3 pe-4 d-flex flex-column">
                    {props.state.messages.map((message) => {
                        return <div className={( (message.name == props.account) ? 'self-message' : 'peer-message')}>
                            <p className="account text-lg-small mt-3">{message.name}</p>
                            <h5 className="message p-2 mb-1 rounded">{message.message}</h5>
                        </div>
                    })}
                    <div ref={messagesEndRef}/>
                
                </div>

            </div>

            <div className="p-4 m-0 sendbounds d-flex flex-row">
                <input  className="sendbox bg-lilac border rounded h-100 w-85 p-4 align-text-top"
                        value={props.formState.message}
                        onChange={(e) => { props.setForm({ ...props.formState, message: e.target.value  }) }}
                        onKeyDown={ (e) => {
                            if (e.key === 'Enter') {
                                sendPrompt();
                              }
                        }}
                >
                </input>
                <div
                    className="ps-2 h-100 w-20 user-select-none cursor-pointer d-flex justify-content-center align-items-center"
                    onClick={() => {
                                if (prompt.length>0){
                                    sendPrompt();
                                }
                    }}
                >
                    <p className="text-denim text-lg-h2 p-4 bg-lilac border rounded">SEND</p>
                </div>
            </div>

        </div>
    )
}
export default Messaging