import {Link} from "react-router-dom"
import './Landing.css'

const Landing = () => {

    return (
        <div className="landing p-5 d-flex justify-content-center align-items-center flex-column">
            <p className="font-hyper text-dark-purple text-lg-h1 glow user-select-none mb-4">DEMESSAGE</p>
            <p className="text-lg-h3 w-35 text-center text-orchid user-select-none">A fully decentralized chat app powered by Ethereum Authentication and Gun.js</p>
            <div className="d-flex flex-row">
                <div className="m-5 option bg-orchid text-lilac p-2 border rounded border-purple">
                    <Link className="text-purple user-select-none" style={{textDecoration: 'none'}} to="/app">Open App</Link>
                </div>
                <div className="m-5 option bg-orchid text-lilac p-2 border rounded border-purple">
                    <Link className="text-purple user-select-none" style={{textDecoration: 'none'}} to="/test">Test Page</Link>
                </div>
            </div>
        </div>
    )
}
export default Landing