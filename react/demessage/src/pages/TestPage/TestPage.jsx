import "./TestPage.css"
import logo from "../../logo.svg"

import HeaderBar from "../../components/HeaderBar"

const TestPage = () => {

    return (
    <div className="d-flex flex-column h-100">
        <HeaderBar/>

        <div className="App d-flex flex-row w-100 overflow-scroll">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                DeMessage Boilerplate Application
                </p>

            </header>
            <div className='style-guide bg-lilac text-denim'> 
                <h1 className='font-hyper'>DeMessage</h1>
                <p>p tag</p>
                <h2>Default Font: Roboto</h2>
                <h3 className='font-hyper'>Artsy Font: Hyperdimension</h3>
                <h1>h1</h1>
                <h2>h2</h2>
                <h3>h3</h3>
                <h4>h4</h4>
                <h5>h5</h5>
                <h6>h6</h6>
                <button>button</button>
            </div>
            <div className='d-flex flex-column justify-content-stretch border border-denim'>
                <div className='bg-turquoise h-100 d-flex align-items-center justify-content-center px-6'>
                <p>Turquoise: #4cbfa6</p>
                </div>
                <div className='bg-lilac h-100 d-flex align-items-center justify-content-center px-6'>
                <p>Lilac: #f6ebf4</p>
                </div>
                <div className='bg-purple h-100 d-flex align-items-center justify-content-center px-6'>
                <p className='text-white'>Purple: #482673</p>
                </div>
                <div className='bg-orchid h-100 d-flex align-items-center justify-content-center px-6'>
                <p>Orchid: #ed0b70</p>
                </div>
                <div className='bg-denim h-100 d-flex align-items-center justify-content-center px-6'>
                <p className='text-white'>Denim: #301008</p>
                </div>
            </div>
        </div>
    </div>
    )
}
export default TestPage