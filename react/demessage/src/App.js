// routing
import { BrowserRouter, Routes, Route } from "react-router-dom"

// pages
import Landing from './pages/Landing';
import TestPage from './pages/TestPage';
import Application from "./pages/Application";


// styles
import "./App.css"



function App() {
  return (
      <div className="page">
        <BrowserRouter >
          <Routes>
            <Route index element={<Landing/>} />
            <Route path="test" element={<TestPage/>} />
            <Route path="app" element={<Application/>} />
          </Routes>
        </BrowserRouter>
      </div>
  );
}

export default App;
