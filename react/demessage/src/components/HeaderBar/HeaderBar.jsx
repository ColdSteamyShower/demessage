// modules
import { useNavigate } from "react-router-dom";

//styles
import "./HeaderBar.css"

const HeaderBar = () => {
    let navigate = useNavigate();

    return (
        <div className="row w-100 m-0 bg-dark-purple header d-flex justify-content-start zindex-sticky bottom-shadow">
            <div className="h-100 d-flex align-items-center">
                <h1 className="text-orchid font-hyper ms-3 cursor-pointer text-lg-h1 ml-4 title"
                    onClick={() => {navigate("/")}}
                >DEMESSAGE</h1>
            </div>
        </div>
    )
}
export default HeaderBar