# DeMessage

A decentralized messaging service that runs on Ethereum

## Instructions

- Due to Vercel issues, the app is currently only available locally
- Requires Node.js (> v15) and NPM
- cd into `~/react/demessage/`
- Run `yarn start`
- To open more instances of the app for testing, access http://localhost:3000
- In case of Heroku failures, open a separate console and run `yarn start` in directory `~/heroku/`

## Current Features

- Landing Page
- Testing / Style Page
- Chat Interface
- Provided Public Server
- MetaMask login (install MetaMask to browser)
- Local Gun.js server (in case of Heroku or Vercel interruptions)
- Local Gun.js Storage

## Current Bugs

- Occasionally, Vercel or Heroku will shut down, leaving the application disconnected from its network. In this case, run a local Gun.js server in ~/heroku with `node server.js`
- Due to interference between ETH wallet IDs and Gun.js storage, contacts and usernames were ommitted in this version. There is a public text channel provided to showcase the functionality of the app
- Occasionally MetaMask authorization will not go through correctly while the app itself changes state to believe it is still trying to log in, leaving the user not logged in on the app. In this case, just reload the page.
